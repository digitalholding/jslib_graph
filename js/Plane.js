const {Axis} = require("../js/Axis");
const {Line} = require("../js/Line");
const {Point} = require("../js/Point");
require('prototype-extensions/js/PrototypeExtensions');

/**
 * Płaszczyzna, an której rysujemy wykresy
 */
class Plane {
    constructor(config, createGraphLine) {
        this.drawWaiting = false;
        this.createGraphLine = createGraphLine;
        this._makeDefaults();
        this._elements = [];

        //osie
        this._mouseProjectionX = null;
        this._mouseProjectionY = null;
        this._xs = [new Axis('x', null, createGraphLine)];
        this._ys = [new Axis('y', null, createGraphLine)];

        if(createGraphLine) {
            this._setAxisEvents(this._xs[0]);
            this._setAxisEvents(this._ys[0]);
        }

        this._config.x = this._xs[0].parentConfig;
        this._config.y = this._ys[0].parentConfig;
        this._svg = null;

        if (config) {
            this.config = config;
        }
    }

    get xs() {
        return this._xs[0];
    }

    get ys() {
        return this._ys[0];
    }

    set xs(value) {
        this._xs[0] = value;
    }

    set ys(value) {
        this._ys[0] = value;
    }

    get config() {
        return this._config;
    }

    /**
     * Ustawiamy konfigurację. Możemy przekazać tylko wybrane parametry, to wtedy pozostałe zostaną bez zmian.
     * Możemy też dobrać się tak do konfiguracji osi (które są swoimi własnymi obiektami kalsy Axis)
     * @param a
     */
    set config(a) {

        //x i y to osie, więc ich konfig przekazujemy do obiektów klasy Axis (ang. oś)
        if (a.x) {
            Object.assign(this._config.x, a.x);
            delete a.x;
        }
        if (a.y) {
            Object.assign(this._config.y, a.y);
            delete a.y;
        }
        if (a.marginX) {
            this._config.x.marginStart = a.marginX;
            this._config.x.marginEnd = a.marginX;
        }
        if (a.marginY) {
            this._config.y.marginStart = a.marginY;
            this._config.y.marginEnd = a.marginY;
        }
        Object.assign(this._config, a);
        this.draw();
    }

    /**
     *
     * @returns {*} zwraca element svg, który możemy wstawić do htmla
     */
    get svg() {
        if (!this._svg && this.createGraphLine) {
            this._makeSvg();
        }

        //this.drawTimeout();
        return this._svg;
    }

    _prepareTooltip() {
        if (!this._tooltipElement && this.createGraphLine) {
            this._tooltipElement = this.svg.ownerDocument.createElementNS("http://www.w3.org/2000/svg", 'g');
            this._tooltipElementText = this.svg.ownerDocument.createElementNS("http://www.w3.org/2000/svg", 'foreignObject');
            this._tooltipElementText.setAttribute('x', 0);
            this._tooltipElementText.setAttribute('y', -20);
            this._tooltipElementText.setAttribute('text-anchor', 'middle');
            this._tooltipElementText.setAttribute('alignment-baseline', 'baseline');
            this._tooltipElementText.setAttribute('pointer-events', 'none');
            this._tooltipElement.appendChild(this._tooltipElementText);
            this.svg.appendChild(this._tooltipElement);
            this._tooltipElementText.textContent = '';
        }
        return this._tooltipElement;
    }

    _prepareTooltipPoint() {
        if (!this._tooltipPointElement && this.createGraphLine) {
            this._tooltipPointElement = this.svg.ownerDocument.createElementNS("http://www.w3.org/2000/svg", 'g');
            this._tooltipPointElementText = this.svg.ownerDocument.createElementNS("http://www.w3.org/2000/svg", 'foreignObject');
            this._tooltipPointElementText.setAttribute('y', 25);
            this._tooltipPointElementText.setAttribute('x', 25);
            this._tooltipPointElementText.setAttribute('text-anchor', 'start');
            this._tooltipPointElementText.setAttribute('alignment-baseline', 'baseline');
            this._tooltipPointElementText.setAttribute('pointer-events', 'none');
            this._tooltipPointElement.appendChild(this._tooltipPointElementText);
            this.svg.appendChild(this._tooltipPointElement);
            this._tooltipPointElementText.textContent = '';
            this._tooltipPointElement.style.display = 'none';
        }
        return this._tooltipPointElement;
    }

    _prepeareMouseProjection() {
        if (!this.mouseProjectionAxises && this.createGraphLine) {
            let lineX = document.createElementNS("http://www.w3.org/2000/svg", 'line');
            lineX.classList.add('projection');

            this._mouseProjectionX = lineX;

            let lineY = document.createElementNS("http://www.w3.org/2000/svg", 'line');
            lineY.classList.add('projection');

            this._mouseProjectionY = lineY;

            this._svg.insertBefore(this._mouseProjectionX, this._svg.firstChild);
            this._svg.insertBefore(this._mouseProjectionY, this._svg.firstChild);

            this._hideMouseProjectionLines();
        }
    }

    _moveMouseProjectionLines(positionX, positionY) {
        if (positionX && positionY) {
            this._mouseProjectionX.setAttribute('x1', this.xs.marginStart);
            this._mouseProjectionX.setAttribute('y1', positionY);
            this._mouseProjectionX.setAttribute('x2', positionX + 20);
            this._mouseProjectionX.setAttribute('y2', positionY);

            this._mouseProjectionY.setAttribute('x1', positionX);
            this._mouseProjectionY.setAttribute('y1', this.ys.size - this.ys.marginStart);
            this._mouseProjectionY.setAttribute('x2', positionX);
            this._mouseProjectionY.setAttribute('y2', positionY - 20);
        }
    }

    _hideMouseProjectionLines() {
        if (this._mouseProjectionX && this._mouseProjectionY) {
            this._mouseProjectionX.style.display = "none";
            this._mouseProjectionY.style.display = "none";
        }
    }

    _showMouseProjectionLines() {
        if (this._mouseProjectionX && this._mouseProjectionY) {
            this._mouseProjectionX.style.display = "block";
            this._mouseProjectionY.style.display = "block";
        }
    }

    _makeDefaults() {
        this._config = {
            x: {},
            y: {},
            tooltipPointEnabled: false,
            tooltipPointFunction: (x, y) => "X= " + x + " Y= " + y,
            mouseProjection: false,
        };
        this._mouseMoveEvents = [];
        this._mouseClickEvents = [];
        this._mouseLeaveEvents = [];
        this._movingElement = null;

        if(this.createGraphLine) {
            window.addEventListener('mousemove', this._windowMouseMove.bind(this));
            window.addEventListener('mouseup', this._windowMouseUp.bind(this));
        }
    }

    _makeSvg() {
        this._svg = document.createElementNS("http://www.w3.org/2000/svg", 'svg');
        this._xs.forEach(s => this._svg.appendChild(s.svg));
        this._ys.forEach(s => this._svg.appendChild(s.svg));
        this._elementsGroup = document.createElementNS("http://www.w3.org/2000/svg", 'g');
        this._elementsGroupClipPath = document.createElementNS("http://www.w3.org/2000/svg", 'clipPath');
        this._elementsGroupClipPath.id = (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
        this._elementsGroupClipPathRectangle = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
        this._elementsGroupClipPath.appendChild(this._elementsGroupClipPathRectangle);
        this._elementsGroup.appendChild(this._elementsGroupClipPath)
        this._elementsGroup.setAttribute('clip-path', 'url(#' + this._elementsGroupClipPath.id + ')');
        this._svg.appendChild(this._elementsGroup);
        this._svg.addEventListener('resize', this.draw.bind(this));
        addEventListener('resize', this.draw.bind(this));
        this._svg.addEventListener('mousemove', this._onMouseMove.bind(this));
        this._svg.addEventListener('mouseleave', this._onMouseLeave.bind(this));
        this._svg.addEventListener('mouseup', this._onMouseClick.bind(this));

        this._prepeareMouseProjection();
    }

    /**
     * przerysowuje cały wykres
     * @returns {boolean} czy się udało
     */
    draw() {
        if (!this.drawWaiting) {
            this.drawWaiting = true;
            window.requestAnimationFrame(() => this._draw());
        }
    }


    _draw() {
        if(this.createGraphLine) {
            this.drawWaiting = false;
            if (!this._svg) {
                return false;
            }
            let svgWidth = this._svg.clientWidth + 50;
            let svgHeight = this._svg.clientHeight;
            if (!svgWidth || !svgHeight) {
                return;
            }
            let oppositeSumX = 0;
            let oppositeSumY = 0;
            this._xs.forEach((axis, index) => {
                axis.config = {
                    oppositeSide: index > 0,
                    marginPerpendicular: (index == 0) ? (this.ys.config.marginStart + axis.config.thickness) : this.ys.config.marginEnd + oppositeSumX
                };
                oppositeSumX += axis.config.thickness;
            });
            this._ys.forEach((axis, index) => {
                axis.config = {
                    oppositeSide: index > 0,
                    marginPerpendicular: (index == 0) ? (this.xs.config.marginStart + axis.config.thickness) : this.xs.config.marginEnd + oppositeSumY
                };
                oppositeSumY += axis.config.thickness;
            });
            this._refreshPrependicularThickness();
            this._xs.forEach((axis, index) => {
                let xYitleGap = (this._ys.length - 1) * 50 * (-1);
                axis._draw(svgWidth, svgHeight, xYitleGap);
            });
            this._ys.forEach((axis, index) => {
                axis._draw(svgWidth, svgHeight);
            });
            if (this.xs.needRedrawGrid || this._gridXAxis != this.xs || this.ys.needRedrawGrid || this._gridYAxis != this.ys) {
                if (this.xs.config.grid) {
                    this._drawGridX(svgHeight);
                }
                if (this.ys.config.grid) {
                    this._drawGridY(svgWidth);
                }
            }
            for (let element of this._elements) {
                if (!element.xs)
                    element.xs = this.xs

                if (!element.ys)
                    element.ys = this.ys

                element._draw(element.xs, element.ys, svgWidth, svgHeight);
            }
            this._elementsGroupClipPathRectangle.style.width = this.xs.sizeWithoutMargin;
            this._elementsGroupClipPathRectangle.style.height = this.ys.sizeWithoutMargin;
            this._elementsGroupClipPathRectangle.style.x = this.xs.marginStart;
            this._elementsGroupClipPathRectangle.style.y = this.ys.marginEnd;
        }

        return true;
    }

    /**
     * przerysowuje cały wykres, ale nie od razu
     */
    drawTimeout() {
        if (this.drawTimeoutInt) {
            clearTimeout(this.drawTimeoutInt);
        }
        this.drawTimeoutInt = setTimeout(() => {
            this.draw();
        }, 1);
    }

    /**
     * dodaje element do wykresu
     * @param element linia (Axis) lub punkt (Point)
     */
    add(element) {
        if(this.createGraphLine) {
            this.svg;

            if (element instanceof (Point)) {
                this._elementsGroup.appendChild(element.svg);
            } else {
                this._elementsGroup.prepend(element.svg);
            }

            this._elements.push(element);
            element.svg.onmousedown = this._onElementDown.bind(this, element.svg);
            element.svg.onmouseup = this._onElementUp.bind(this, element.svg);
            element.parentPlane = this;
            this.draw();
        }
    }

    /**
     * odwrotność add
     * @param element
     */
    remove(element) {
        if(this.createGraphLine) {
            this.svg;

            let index = this._elements.indexOf(element);
            element.parentPlane = null;
            element.svg.remove();
            if (index >= 0) {
                this._elements.splice(index, 1);
            }

            this.draw();
        }
    }

    /**
     * dodaje event na poruszanie myszą po wykresie
     * @param fun funkcja obsługi zdażenia
     */
    mouseMove(fun) {
        this._mouseMoveEvents.push(fun);
    }

    mouseClick(fun) {
        this._mouseClickEvents.push(fun);
    }

    /**
     * dodaje event na poruszanie wyjechania myszą za wykres
     * @param fun funkcja obsługi zdażenia
     */
    mouseLeave(fun) {
        this._mouseLeaveEvents.push(fun);
    }

    _onMouseLeave(event) {
        if (this._tooltipPointElement) {
            this._tooltipPointElement.style.display = 'none';
        }

        if (this._mouseIsInside) {
            this._mouseIsInside = false;

            this._onMouseLeavePlane();
        }
    }

    static createMultilineText(text, element) {
        while (element.firstChild) {
            element.removeChild(element.firstChild);
        }

        let lines = text.split('\r\n');
        for (let line of lines) {
            let div = document.createElementNS("http://www.w3.org/2000/svg", 'div');
            div.textContent = line;
            div.setAttribute('x', 0);
            div.setAttribute('dy', '1em');
            element.appendChild(div);
        }
        return {linesLength: lines.length};
    }

    static updateMultilineForeign(text, foreign) {
        while (foreign.firstChild) {
            foreign.removeChild(foreign.firstChild);
        }
        if (text instanceof Node) {
            foreign.appendChild(text);
        } else {
            foreign.textContent = text;
        }
    }

    _onMouseMove(event) {
        let x = this.xs.createValue(this.xs.getPosByPixels(event.offsetX, event.altKey));
        let y = this.ys.createValue(this.ys.getPosByPixels(event.offsetY, event.altKey));
        let newEvent = new Event('graphMouseMove');
        newEvent.x = x;
        newEvent.y = y;
        newEvent.oryginalEvent = event;
        newEvent.isOnPlane = this._mouseIsInside;
        newEvent.values = [];
        for (let element of this._elements) {
            if (element instanceof Line) {
                newEvent.values.push({value: element.fun(x), element: element});
            }
        }
        for (let e of this._mouseMoveEvents) {
            e.call(this, newEvent);
        }
        this._prepareTooltip();
        this._prepareTooltipPoint();
        this._tooltipElement.style.transform = `translate(${event.offsetX}px,${event.offsetY}px)`;


        let findedTarget = this.findElementByTarget(event);
        this._tooltipElementText.textContent = '';

        if (findedTarget && findedTarget.title) {
            if (findedTarget.titleAsSVG) {
                this._tooltipElementText.innerHTML = findedTarget.title;
                this._tooltipElementText.setAttribute('y', -20 * findedTarget.titleLines);
            } else {
                let {linesLength} = Plane.createMultilineText(findedTarget.title, this._tooltipElementText);
                this._tooltipElementText.setAttribute('y', -20 * linesLength);
            }
        }

        if (this.config.tooltipPointEnabled) {
            this._tooltipPointElement.style.transform = `translate(${event.offsetX}px,${event.offsetY}px)`;
            Plane.updateMultilineForeign(this.config.tooltipPointFunction(x, y), this._tooltipPointElementText);
        }

        if (this.config.mouseProjection) {
            this._moveMouseProjectionLines(event.offsetX, event.offsetY);
        } else {
            this._hideMouseProjectionLines();
        }

        if (this._isEventOnPlane(event)) {
            if (!this._mouseIsInside) {
                this._mouseIsInside = true;

                this._onMouseEnterPlane();
            }
        } else {
            if (this._mouseIsInside) {
                this._mouseIsInside = false;

                this._onMouseLeavePlane();
            }
        }
    }

    _onMouseEnterPlane() {
        if (this.config.mouseProjection) {
            this._showMouseProjectionLines();
        }

        if (this._tooltipPointElement) {
            this._tooltipPointElement.style.display = '';
        }
    }

    _onMouseLeavePlane() {
        this._hideMouseProjectionLines();

        if (this._tooltipPointElement) {
            this._tooltipPointElement.style.display = 'none';
        }
    }

    _isEventOnPlane(event) {
        if (event.offsetX < this.xs.marginStart) return false;
        if (event.offsetX > this.xs.size - this.xs.marginEnd) return false;
        if (event.offsetY < this.ys.marginEnd) return false;
        if (event.offsetY > this.ys.size - this.ys.marginStart) return false;
        if (event.target.findParent(x => x == this._svg) == null) return false;
        return true;
    }

    _onMouseClick(event) {
        if (!this._isEventOnPlane(event)) return;
        if (event.button != 0) return;
        if (this._movingElement) return;
        let x = this.xs.createValue(this.xs.getPosByPixels(event.offsetX, event.altKey));
        let y = this.ys.createValue(this.ys.getPosByPixels(event.offsetY, event.altKey));
        let newEvent = new Event('graphMouseClick');
        newEvent.x = x;
        newEvent.y = y;
        newEvent.oryginalEvent = event;
        newEvent.getByAxis = (axis) => {
            if (axis.name == 'x') return axis.getPosByPixels(event.offsetX, event.altKey);
            else return axis.getPosByPixels(event.offsetY, event.altKey);
        }
        newEvent.values = [];
        for (let e of this._mouseClickEvents) {
            e.call(this, newEvent);
        }
    }

    findElementByTarget(event) {
        let target = event.target
        while (target) {
            if (target.classList && target.classList.contains('projection')) return null;
            let finded = this._elements.find(x => x.svg == target)
            if (finded)
                return finded;
            target = target.parentNode;
        }
        return null;
    }

    _onElementDown(sender, event) {
        this._movingElement = {x: event.screenX, y: event.screenY, element: sender};
        this._movingElementEvent(event, 'start');
    }

    _onElementUp(sender, event) {
        if (this._movingElement) {
            this._movingElementEvent(event, 'end');
            this._movingElement = null;
            event.stopImmediatePropagation();
        }
    }

    _windowMouseUp(event) {
        if (event.button != 0) return;
        if (this._movingElement) {
            this._movingElementEvent(event, 'end');
            this._movingElement = null;
            event.stopImmediatePropagation();
        }
    }

    _movingElementEvent(event, type) {
        let xPX = event.pageX - this.svg.clientLeft - this.svg.parentNode.offsetLeftFull;
        let yPX = event.pageY - this.svg.clientTop - this.svg.parentNode.offsetTopFull;
        let x = this.xs.createValue(this.xs.getPosByPixels(xPX, event.altKey));
        let y = this.ys.createValue(this.ys.getPosByPixels(yPX, event.altKey));
        let newEvent = new Event('moved');
        newEvent.x = x;
        newEvent.y = y;
        newEvent.isOnPlane = this._isEventOnPlane(event);
        newEvent.oryginalEvent = event;
        newEvent.getByAxis = (axis) => {
            if (axis.name == 'x') return axis.getPosByPixels(event.offsetX, event.altKey);
            else return axis.getPosByPixels(event.offsetY, event.altKey);
        }
        newEvent.moveType = type;
        var element = this._elements.find(e => e.svg == this._movingElement.element);
        if (element) {
            element.doMoving(newEvent);
        }
    }

    _windowMouseMove(event) {
        if (this._movingElement) {
            this._movingElementEvent(event, 'move');
        }
    }

    _renderParamsEqual(elem, axis) {
        return elem.dataset.start == axis.config.start && elem.dataset.end == axis.config.end && elem.dataset.step == axis.config.step && elem.dataset.size == axis.size;
    }

    _renderParamsSet(elem, axis) {
        elem.dataset.start = axis.config.start;
        elem.dataset.end = axis.config.end;
        elem.dataset.step = axis.config.step;
        elem.dataset.size = axis.size;
    }

    _drawGridX(height) {
        if (!this._gridX && this.createGraphLine) {
            this._gridX = document.createElementNS("http://www.w3.org/2000/svg", 'g');
            this.svg.prepend(this._gridX);
        }
        this.xs.redrawedGrid();
        this._gridXAxis = this.xs;

        while (this._gridX.firstChild) this._gridX.firstChild.remove();
        let steps = this.xs.getUnitsSteps(2);

        if(this.createGraphLine) {
            let pos, main;
            for ({pos, main} of steps) {
                let line = document.createElementNS("http://www.w3.org/2000/svg", 'line');

                line.setAttribute('x1', pos);
                line.setAttribute('y1', this.ys.marginEnd);
                line.setAttribute('x2', pos);
                line.setAttribute('y2', height - this.ys.marginStart);
                line.classList.add('grid');
                if (main) {
                    line.classList.add('gridMain');
                }
                this._gridX.appendChild(line);
            }
        }
    }

    _drawGridY(width) {
        if (!this._gridY && this.createGraphLine) {
            this._gridY = document.createElementNS("http://www.w3.org/2000/svg", 'g');
            this.svg.prepend(this._gridY, this.ys);
        }
        this.ys.redrawedGrid();
        this._gridYAxis = this.ys;
        while (this._gridY.firstChild) this._gridY.firstChild.remove();
        let steps = this.ys.getUnitsSteps(2);

        if(this.createGraphLine) {
            let pos, main;
            for ({pos, main} of steps) {
                let line = document.createElementNS("http://www.w3.org/2000/svg", 'line');

                line.setAttribute('x1', this.xs.marginStart);
                line.setAttribute('y1', pos);
                line.setAttribute('x2', width - this.xs.marginEnd);
                line.setAttribute('y2', pos);
                line.classList.add('grid');
                if (main) {
                    line.classList.add('gridMain');
                }
                this._gridY.prepend(line);
            }
        }
    }

    addAxis(axis) {
        if (axis.name == 'x') {
            axis.parentConfig = this._config.x;
            this._xs.push(axis);
            this.sortAxises("x");
        } else {
            axis.parentConfig = this._config.y;
            this._ys.push(axis);
            this.sortAxises("y");
        }
        this._setAxisEvents(axis);
        if (this._svg)
            this._svg.appendChild(axis.svg);
    }

    _setAxisEvents(axis) {
        if(this.createGraphLine) {
            axis.svg.addEventListener('mouseenter', () => {
                for (let item of this._elements) {
                    if (item.ys != axis && item.xs != axis) {
                        item.svg.classList.add('previewInactive');
                    }
                }
            });
            axis.svg.addEventListener('mouseleave', () => {
                for (let item of this._elements) {
                    item.svg.classList.remove('previewInactive');
                }
            });
        }
    }

    sortAxises(name, mainAxis = null) {
        let array = name == 'x' ? this._xs : this._ys;

        var sortTable = [];
        var sortedAxises = [];

        if (mainAxis) {
            sortedAxises.push(mainAxis);
        }

        for (let axis of array) {
            if (!sortTable[axis.config.priority])
                sortTable[axis.config.priority] = [];

            if (axis != mainAxis) {
                sortTable[axis.config.priority].push(axis);
            }
        }

        for (let i = sortTable.length - 1; i >= 0; i--) {
            if (sortTable[i]) {
                for (let axis of sortTable[i]) {
                    sortedAxises.push(axis);
                }
            }
        }

        if (name == 'x') {
            this._xs = sortedAxises;
        } else {
            this._ys = sortedAxises
        }
    }

    removeAxis(axis) {
        let xIndex = this._xs.indexOf(axis);
        if (xIndex >= 0)
            this._xs.splice(xIndex, 1);
        let yIndex = this._ys.indexOf(axis);
        if (yIndex >= 0)
            this._ys.splice(yIndex, 1);
        axis.svg.remove();
    }

    containsAxis(axis) {

        if (this._xs.indexOf(axis) >= 0)
            return true;
        else if (this._ys.indexOf(axis) >= 0)
            return true;
        else
            return false;
    }

    _refreshPrependicularThickness() {
        this._refreshPrependicularThicknessOnce(this._ys, this._xs);
        this._refreshPrependicularThicknessOnce(this._xs, this._ys);
    }

    _refreshPrependicularThicknessOnce(a, b, startIsOpposite) {
        let start = 0;
        let end = 0;
        for (let aItem of a) {
            if (aItem.config.oppositeSide) {
                end += aItem.config.thickness;
            } else {
                start += aItem.config.thickness;
            }
        }
        for (let bItem of b) {
            bItem.marginAdditionalStart = start;
            bItem.marginAdditionalEnd = end;
        }
    }

    setMainAxis(axis) {
         if (axis && !this.containsAxis(axis))
            this.addAxis(axis);

        this.sortAxises(axis.name, axis);
    }
}

module.exports.Plane = Plane;