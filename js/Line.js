class Line {
    constructor(fun, config, createGraphLine = true) {
        this.fun = fun;
        this.createGraphLine = createGraphLine;

        this._makeDefaults();
        if (config) {
            this.config = config;
        }
    }

    get config() {
        return this._config;
    }

    /**
     * Ustawiamy konfigurację. Możemy przekazać tylko wybrane parametry, to wtedy pozostałe zostaną bez zmian.
     * @param a
     */
    set config(a) {
        Object.assign(this._config, a);
    }

    /**
     *
     * @returns {SVGElement} element svg typu Path
     */
    get svg() {
        if (!this._svg && this.createGraphLine) {
            this._makeSvg();
        }
        return this._svg;
    }

    _makeDefaults() {
        this._config = {start: -Infinity, end: Infinity, step: 1, className: '', resolution: 3};
        this._moved = [];
    }

    _makeSvg() {
        this._svg = document.createElementNS("http://www.w3.org/2000/svg", 'path');
    }

    _draw(xs, ys) {
        if(this.createGraphLine) {
            this.svg.setAttribute('class', this.config.className);
            let startX = Math.max(this._config.start, xs.start);
            let endX = Math.min(this._config.end, xs.end);
            let startPX = xs.getPixels(startX);
            let endPX = xs.getPixels(endX);
            let path = this._makePath(xs, ys, startPX, endPX);
            this._svg.setAttribute('d', path);
            if (this._config.color)
                this.svg.style.stroke = this._config.color;
        }
    }

    _makePath(xs, ys, startPX, endPX) {
        //ile iteracji pętli
        let path = '';
        let max = (endPX - startPX) / this.config.step;
        let lastWasGood = false;
        let getPosByPixels = xs.getPosByPixelsOptimized();
        let getPixels = ys.getPixelsOptimized();
        let step = this.config.step;
        let resolution = this.config.resolution;
        for (let i = 0; i < max + resolution; i += resolution) {
            if (i > max) i = max;
            let posPX = startPX + i * step;
            let letter;
            if (lastWasGood) {
                letter = 'L';
            } else {
                letter = 'M';
            }
            lastWasGood = false;

            let yPX = getPixels(this.fun(getPosByPixels(posPX)));
            if (!Number.isFinite(yPX)) {
                continue;
            }
            if (yPX > 1000000) {
                yPX = 1000000;
            } else if (yPX < -1000000) {
                yPX = -1000000;
            }
            path += letter + posPX + '  ' + yPX + ' ';
            lastWasGood = true;
            if (i == max) break;//na wszelki wypadek, jakby był problem z zaokrąglaniem floatów
        }
        return path;
    }

    /**
     * zdażenie, że przeciągneliśmy myszą wykres
     * @param fun
     */
    moved(fun) {
        this._moved.push(fun);
    }

    /**
     * wywołwane z klasy Plane, ze ta linia jest przeciagana
     * @param event
     */
    doMoving(event) {
        for (let e of this._moved) {
            e.call(this, event);
        }
    }

    remove() {
        if (!this.parentPlane)
            throw new Error('object is not on plane');
        this.parentPlane.remove(this);
    }
}

module.exports.Line = Line;