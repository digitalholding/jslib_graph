class Point {
    constructor(x, y, config = {}) {
        this.x = x;
        this.y = y;
        this._makeDefaults();
        if (config) {
            this.config = config;
        }
    }

    get config() {
        return this._config;
    }

    /**
     * Ustawiamy konfigurację. Możemy przekazać tylko wybrane parametry, to wtedy pozostałe zostaną bez zmian.
     * @param a
     */
    set config(a) {
        Object.assign(this._config, a);
    }

    /**
     *
     * @returns {SVGElement} element svg typu g
     */
    get svg() {
        if (!this._svg) {
            this._makeSvg();
        }
        return this._svg;
    }

    get svgPoint() {
        if (!this._svgPoint) {
            this._makeSvg();
        }
        return this._svgPoint;
    }

    _makeDefaults() {
        this._config = {className: ''};
        this._moved = [];
    }

    _makeSvg() {
        this._svg = document.createElementNS("http://www.w3.org/2000/svg", 'g');
        this._svgPoint = document.createElementNS("http://www.w3.org/2000/svg", 'g');
        this._svg.appendChild(this._svgPoint);
        let remove = this._svg.remove;
        this._svg.remove = () => {
            this._clearProjection();
            remove.call(this._svg);
        };
    }

    _draw(xs, ys, width, height) {
        this.svg.setAttribute('class', this.config.className);
        let xPX = xs.getPixels(this.x);
        let yPX = ys.getPixels(this.y);

        this._svgPoint.style.transform = 'translate(' + xPX + 'px, ' + yPX + 'px)';

        if (this.config.projection)
            this._drawProjection(xs, ys, width, height);
        else
            this._clearProjection();
    }

    _drawProjection(xs, ys, width, height) {
        if (!this._projectionX) {
            this._projectionX = document.createElementNS("http://www.w3.org/2000/svg", 'line');
            this._projectionX.classList.add('projection');
            this.svg.parentNode.parentNode.prepend(this._projectionX);
        }
        if (!this._projectionY) {
            this._projectionY = document.createElementNS("http://www.w3.org/2000/svg", 'line');
            this._projectionY.classList.add('projection');
            this.svg.parentNode.parentNode.prepend(this._projectionY);
        }

        let xPX = xs.getPixels(this.x);
        let yPX = ys.getPixels(this.y);

        if (ys.getConfigValue('oppositeSide')) {
            this._projectionX.setAttribute('x1', width - ys.getConfigValue('marginPerpendicular'));
            this._projectionX.setAttribute('y1', yPX);
        } else {
            this._projectionX.setAttribute('x1', xs.marginStart);
            this._projectionX.setAttribute('y1', yPX);
        }

        this._projectionX.setAttribute('x2', xPX);
        this._projectionX.setAttribute('y2', yPX);

        if (xs.getConfigValue('oppositeSide')) {
            this._projectionY.setAttribute('x1', xPX);
            this._projectionY.setAttribute('y1', ys.marginStart);
        } else {
            this._projectionY.setAttribute('x1', xPX);
            this._projectionY.setAttribute('y1', height - ys.marginStart);
        }
        this._projectionY.setAttribute('x2', xPX);
        this._projectionY.setAttribute('y2', yPX);

        let axisYColor = ys.getConfigValue('color');
        if (axisYColor) {
            this._projectionX.style.stroke = axisYColor;
            this._projectionX.style.opacity = 0.8;
        }

        let axisXColor = xs.getConfigValue('color');
        if (axisXColor) {
            this._projectionY.style.stroke = axisXColor;
            this._projectionY.style.opacity = 0.8;
        }
    }

    _clearProjection() {
        if (this._projectionX) {
            this._projectionX.remove();
            delete this._projectionX;
        }
        if (this._projectionY) {
            this._projectionY.remove();
            delete this._projectionY;
        }
    }

    /**
     * zdażenie, że przeciągneliśmy myszą wykres
     * @param fun
     */
    moved(fun) {
        this._moved.push(fun);
    }

    /**
     * wywołwane z klasy Plane, ze ten punkt jest przeciągany
     * @param event
     */
    doMoving(event) {
        for (let e of this._moved) {
            e.call(this, event);
        }
    }

    remove() {
        if (!this.parentPlane)
            throw new Error('object is not on plane');
        this._clearProjection();
        this.parentPlane.remove(this);
    }

}

module.exports.Point = Point;