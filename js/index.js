import {Plane} from "./Plane";
import {Line} from "./Line";
import {Point} from "./Point";

//oto demo. Tworzymy nowy wykres
window.p = new Plane();

p.config = {marginX: 60, y: {step: 50}};

//dodajemy kilka wykresów, tutaj np. parabola
p.add(new Line(x => x * x));

//możemy dodać gdzie wykres się zaczyna i kończy (domyślnie od -nieskończoności do +nieskończoności) i możemy dodać kalsę, żeby go potem ostylować
p.add(new Line(x => -x * x, {start: -3, end: 1, className: 'fajna'}));

//możemy robić wykresy stałe
p.add(new Line(x => 0));
p.add(new Line(x => 1));

//jeśli wykres zwróci NaN (bo np. pierwiastek z liczby ujemnej) to rysowane jest jako przerwa w wykresie
p.add(new Line(x => 3 + Math.sqrt(x * x - 4 * x + 2)));
//funkcja wykładnicza może być
p.add(new Line(x => Math.pow(2, x - 5)));
//hiperbola
p.add(new Line(x => 5 / x));

//oprócz lini możemy też dodawać punkty. Punkt jest to grupa w svg, do której sami musimy wrzuccić jakieś elementy, ale za to środek będzie na wykresie tam, gdzie ustalimy
let miniCircle = new Point(3, 5);
miniCircle.title = 'test123';
p.add(miniCircle);
let z = document.createElementNS("http://www.w3.org/2000/svg", 'circle');
z.setAttribute('r', 10);
miniCircle.svgPoint.appendChild(z);

document.body.appendChild(p.svg);

//najeżdzając myszą na wykres możemy podejrzweć jaki to punkt
p.mouseMove(event => console.log(event));


//linię możemy też przeciągać. Wtedy odpala się event moved. Niestety sami musimy zatroszczyć się o to, jak nasz wykres się zachowa
//tutaj troszkę matematyki, dzięki której dostajemy rozciągalną parabolę
let moving = new Line(function (x) {
    return this.a * x * x + this.a * this.s * x;
}, {className: 'big'})
moving.a = 1;
moving.s = 2;
moving.moved(function (event) {
    console.log(event);
    this.a = event.y / (event.x * event.x + this.s * event.x);
    p.draw();
})
p.add(moving);

//tutaj dla testów wykres wartości bezwzględnej: przeciągając myszą wierzchołek będzie podażał za myszą
let WarBezw = new Line(function (x) {
    return Math.abs(x - this.x0) + this.y0;
}, {className: 'big'})
WarBezw.x0 = 1;
WarBezw.y0 = 2;
WarBezw.moved(function (event) {
    this.x0 = event.x;
    this.y0 = event.y;
    p.draw();
})
p.add(WarBezw);

