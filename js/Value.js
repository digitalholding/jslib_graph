class Value extends Number {
    constructor(oryginal, oryginalUnit = '', unit = {
        designation: '',
        apply: x => x,
        restore: x => x
    }) {
        super(oryginal);
        if (typeof oryginal == 'number')
            this._oryginal = oryginal;
        else
            this._oryginal = Number.parse(oryginal.toString());
        this._oryginalUnit = oryginalUnit;
        this._unit = unit;
    }

    get oryginal() {
        return this._oryginal;
    }

    get oryginalUnit() {
        return this._oryginalUnit;
    }

    get display() {
        return this._unit.apply(this._oryginal);
    }
    get displayString(){
        return this.display.format(this._unit.decimal_places);
    }

    get unit() {
        return this._unit;
    }

    [Symbol.toPrimitive]() {
        return this._oryginal;
    }

    toString() {
        return this._oryginal + ' ' + this._oryginalUnit;
    }

    displayToString() {
        return this.display + ' ' + this._unit.designation;
    }

    createSimilar(number) {
        return new Value(number, this._oryginalUnit, this._unit);
    }
}

module.exports.Value = Value;