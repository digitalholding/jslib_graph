const {Value} = require("../js/Value");

class Axis {
    constructor(name, config, createGraphLine = true) {
        this.name = name;
        this._config = {};
        this.createGraphLine = createGraphLine;
        this._size = 1;
        this._mouseIn = false;
        this.parentConfig = this.getDefaultConfig();
        this.marginAdditionalStart = 0;
        this.marginAdditionalEnd = 0;

        if (config) {
            this.config = config;
        }
    }

    get config() {
        let ret = {};
        Object.assign(ret, this.parentConfig);
        Object.assign(ret, this._config);
        return ret;
    }

    /**
     * Ustawiamy konfigurację. Możemy przekazać tylko wybrane parametry, to wtedy pozostałe zostaną bez zmian.
     * @param a
     */
    set config(a) {
        Object.assign(this._config, a);
        if (a.unit) {
            this._lastDrawedConfig = '';
            this._lastDrawedGridConfig = '';
        }
    }

    get size() {
        return this._size;
    }

    /**
     * ustawia rozmiar rysunku
     * @param a ilość pikseli na ekranie
     */
    set size(a) {
        this._size = a;
    }

    get sizeWithoutMargin() {
        return this._size - this.marginStart - this.marginEnd;
    }

    getConfigValue(name) {
        if (this._config[name] !== undefined)
            return this._config[name];
        else
            return this.parentConfig[name];
    }

    get marginStart() {
        return this.getConfigValue('marginStart') + this.marginAdditionalStart;
    }

    get marginEnd() {
        return this.getConfigValue('marginEnd') + this.marginAdditionalEnd;
    }

    /**
     *
     * @returns { SVGGElement} element svg typu g
     */
    get svg() {
        if (!this._svg && this.createGraphLine) {
            this._makeSvg();
        }
        return this._svg;
    }

    get _needRedraw() {
        return this._lastDrawedConfig !== this._configHash();
    }

    get needRedrawGrid() {
        return this._lastDrawedGridConfig !== this._configHash();
    }

    get start() {
        let returned;
        if (this.getConfigValue('smartScale')) {
            let unitsInStep = this.getUnitsInStep();
            let standard = Math.floor((this.getConfigValue('start')) / unitsInStep) * unitsInStep;
            let withMargin = Math.floor((this.getConfigValue('start') - 0.5 * unitsInStep) / unitsInStep) * unitsInStep;
            if (standard >= 0 && withMargin <= 0) returned = 0;
            else returned = withMargin
        } else {
            returned = this.getConfigValue('start');
        }
        return Math.max(returned, this.getConfigValue('startLimit'));
    }

    get end() {
        let returned;
        if (this.getConfigValue('smartScale')) {
            let unitsInStep = this.getUnitsInStep();
            let standard = Math.ceil((this.getConfigValue('end')) / unitsInStep) * unitsInStep;
            let withMargin = Math.ceil((this.getConfigValue('end') + 0.5 * unitsInStep) / unitsInStep) * unitsInStep;
            if (standard >= 0 && withMargin <= 0) returned = 0;
            else returned = withMargin
        } else {
            returned = this.getConfigValue('end');
        }
        return Math.min(returned, this.getConfigValue('endLimit'));
    }

    /**
     * zaokrągla jaka ma być podziałka na skali do liczb typu 1,2,5,10,20,50,100,200 itp
     * @param number
     * @returns {number}
     */
    static round(number) {
        let tens = Math.pow(10, Math.ceil(Math.log10(number)));
        if (tens / 5 >= number) {
            return tens / 5;
        }
        if (tens / 2 >= number) {
            return tens / 2;
        }
        return tens;
    }

    getDefaultConfig() {
        return {
            startLimit: Number.NEGATIVE_INFINITY,
            endLimit: Number.POSITIVE_INFINITY,
            start: -10,
            end: 10,
            step: 100,
            type: 'linear',
            marginStart: 10,
            marginEnd: 10,
            marginPerpendicular: 10,
            thickness: 30,
            grid: true,
            labelLengthMax: 7,
            color: '#000000',
            priority: 0,
            defaultUnitName: '',
            unit: {designation: '', designationMathML: '', apply: x => x, restore: x => x}
        };
    }

    _makeSvg() {
        this._svg = document.createElementNS("http://www.w3.org/2000/svg", 'g');
        this._svg.setAttribute('class', 'axis axis' + this.name);
        this._svg.addEventListener('mousemove', this._onMouseMove.bind(this));
        this._svg.addEventListener('mouseleave', this._onMouseLeave.bind(this));
    }

    _onMouseLeave() {
        this._mouseIn = false;
    }

    _onMouseMove() {
        if (!this._mouseIn) {
            this._mouseIn = true;

            this._onMouseEnter();
        }
    }

    _onMouseEnter() {
    }

    _configHash() {
        return JSON.stringify(this.config) + this.size + '|' + this.sizePrependicular + '|' + this.marginAdditionalStart + '|' + this.marginAdditionalEnd;
    }

    _draw(width, height, xTitleGap = 0) {
        if(this.createGraphLine) {
            if (this.name == 'x') {
                this.size = width;
                this.sizePrependicular = height;
            } else {
                this.size = height;
                this.sizePrependicular = width;
            }

            if (!this._needRedraw) return;

            this._lastDrawedConfig = this._configHash();

            while (this.svg.firstChild) this.svg.firstChild.remove();

            if (this.name == 'x') {
                if (this.getConfigValue('oppositeSide'))
                    this.svg.style.transform = 'translateY(' + (this.getConfigValue('marginPerpendicular')) + 'px)';
                else
                    this.svg.style.transform = 'translateY(' + (height - this.getConfigValue('marginPerpendicular')) + 'px)';
            } else {
                if (this.getConfigValue('oppositeSide'))
                    this.svg.style.transform = 'translateX(' + (width - this.getConfigValue('marginPerpendicular')) + 'px)';
                else
                    this.svg.style.transform = 'translateX(' + (this.getConfigValue('marginPerpendicular')) + 'px)';
            }

            let line = document.createElementNS("http://www.w3.org/2000/svg", 'line');
            let rect = document.createElementNS("http://www.w3.org/2000/svg", 'rect');

            rect.classList.add('background')
            line.style.stroke = this.getConfigValue('color');

            if (this.name == 'x') {
                line.setAttribute('x1', this.marginStart);
                line.setAttribute('y1', 0);
                line.setAttribute('x2', width - this.marginEnd);
                line.setAttribute('y2', 0);

                rect.setAttribute('x', this.marginStart);
                rect.setAttribute('y', 0);
                rect.setAttribute('width', width - this.marginEnd - this.marginStart);
                rect.setAttribute('height', this.getConfigValue('thickness'));
                if (this.getConfigValue('oppositeSide')) {
                    rect.setAttribute('y', -this.getConfigValue('thickness'));
                } else {
                    rect.setAttribute('y', 0);
                }
            } else {
                line.setAttribute('x1', 0);
                line.setAttribute('y1', this.marginEnd);
                line.setAttribute('x2', 0);
                line.setAttribute('y2', height - this.marginStart);

                rect.setAttribute('y', this.marginEnd);
                rect.setAttribute('width', this.getConfigValue('thickness'));
                rect.setAttribute('height', height - this.marginStart - this.marginEnd);

                if (this.getConfigValue('oppositeSide')) {
                    rect.setAttribute('x', 0);
                } else {
                    rect.setAttribute('x', -this.getConfigValue('thickness'));
                }
            }

            this.svg.appendChild(rect);
            this.svg.appendChild(line);


            let steps = this.getUnitsSteps();
            //rysujemy podziałkę
            let value, pos, text;

            for ({value, pos, text} of steps) {
                let pos = this.getPixels(value);
                let textElem = document.createElementNS("http://www.w3.org/2000/svg", 'text');
                let miniLine = document.createElementNS("http://www.w3.org/2000/svg", 'line');
                miniLine.style.stroke = this.getConfigValue('color');
                textElem.style.fill = this.getConfigValue('color');
                textElem.textContent = text;

                if (this.name == 'x') {
                    textElem.setAttribute('x', pos);
                    if (this.getConfigValue('oppositeSide'))
                        textElem.setAttribute('y', -20);
                    else
                        textElem.setAttribute('y', 20);
                    miniLine.setAttribute('x1', pos);
                    miniLine.setAttribute('y1', -5);
                    miniLine.setAttribute('x2', pos);
                    miniLine.setAttribute('y2', 5);
                } else {
                    if (this.getConfigValue('oppositeSide'))
                        textElem.setAttribute('x', 5);
                    else
                        textElem.setAttribute('x', -5);
                    textElem.setAttribute('y', pos);

                    miniLine.setAttribute('x1', -5);
                    miniLine.setAttribute('y1', pos);
                    miniLine.setAttribute('x2', 5);
                    miniLine.setAttribute('y2', pos);
                }
                this.svg.appendChild(miniLine);
                this.svg.appendChild(textElem);
            }

            if (this.getConfigValue('oppositeSide'))
                this.svg.classList.add('oppositeSide');
            else
                this.svg.classList.remove('oppositeSide');

            let axisText = document.createElementNS("http://www.w3.org/2000/svg", 'foreignObject');
            let axisTextDiv = document.createElement('div')

            axisTextDiv.style.color = this.getConfigValue('color');

            let unit = this.getConfigValue('unit');

            if (this.getConfigValue('titleHTML')) {
                axisTextDiv.innerHTML = this.getConfigValue('titleHTML');
                if (unit.designation) {
                    let span = document.createElement('span');
                    span.classList.add('axis-unit-span');
                    span.innerHTML = `[${unit.designation}]`;
                    axisTextDiv.appendChild(span);
                }
            } else {
                axisTextDiv.textContent = this.getConfigValue('title');
                if (unit.designation)
                    axisTextDiv.textContent += ' [' + unit.designation + ']';
            }

            axisText.classList.add('title');

            if (this.name == 'x') {
                let menuGapX = width - 150 + xTitleGap;
                axisText.setAttribute('x', menuGapX);
                axisText.setAttribute('y', 24);

                if (-xTitleGap > 0)
                    axisText.setAttribute('width', 50);
                else
                    axisText.setAttribute('width', 0);

                axisText.classList.add('xAxiseUnitMenu');
            } else {
                axisText.setAttribute('x', -this.getConfigValue('marginEnd') + (this.getConfigValue('oppositeSide') ? 60 : 10));
                axisText.setAttribute('y', 0);
                axisText.setAttribute('width', 70);
            }

            let btn = document.createElement('div');
            btn.classList.add('axisBtn');
            axisTextDiv.insertBefore(btn, axisTextDiv.firstChild);
            axisText.appendChild(axisTextDiv);
            this.svg.appendChild(axisText);
        }
    }

    /**
     * co ile podziałka na skali
     * @param multipler
     * @returns {number}
     */
    getUnitsInStep(divider = 1) {
        let range = this.getConfigValue('end') - this.getConfigValue('start');
        let unitsInPixel = range / (this.sizeWithoutMargin);
        let unitsCalcedInPixel = this.getConfigValue('unit').apply(unitsInPixel)
        let unitsCalcedInFullStep = Axis.round(unitsCalcedInPixel * this.getConfigValue('step'));
        let unitsInFullStep = this.getConfigValue('unit').restore(unitsCalcedInFullStep)
        return unitsInFullStep / divider;
    }

    getUnitsSteps(multipler = 1) {
        let ret = [];
        let unitsInFullStep = this.getUnitsInStep();
        let unitsInStep = this.getUnitsInStep(multipler);//co ile podziałka na skali
        let first = unitsInFullStep * Math.ceil(this.start / unitsInFullStep);//pierwsza gruba podziałka
        let maxPixelsAbove = 1;
        let maxAbove = maxPixelsAbove / this.calcPixelsInUnits();
        let i = Math.ceil((this.start - first - maxAbove) / unitsInStep);
        for (; i <= (this.end - first + maxAbove) / unitsInStep; i++) {
            let value = first + unitsInStep * i;
            let pos = this.getPixels(value);
            let valueUnit = this.getConfigValue('unit').apply(value);
            let unitsInStepUnit = this.getConfigValue('unit').apply(unitsInStep);
            let text = valueUnit.toFixed(Math.max(-Math.floor(Math.log10(unitsInStepUnit)), 0));
            if (text.length > this.getConfigValue('labelLengthMax'))
                text = (text * 1).toExponential();
            text = text.replace(',', '.');
            let main = i % multipler == 0;
            ret.push({value, pos, text, main});
        }
        return ret;
    }

    /**
     * zamienia wartość na piksele
     * @param value
     * @returns {number}
     */
    getPixels(value) {
        let pixelsInUnit = this.calcPixelsInUnits();

        if (this.name == 'x')
            return (value - this.start) * pixelsInUnit + this.marginStart;
        else
            return -(value - this.end) * pixelsInUnit + this.marginEnd;
    }

    getPixelsOptimized(value) {
        let pixelsInUnit = this.calcPixelsInUnits();
        let marginStart = this.marginStart;
        let marginEnd = this.marginEnd;
        let start = this.start;
        let end = this.end;

        if (this.name == 'x')
            return value => (value - start) * pixelsInUnit + marginStart;
        else
            return value => -(value - end) * pixelsInUnit + marginEnd;
    }

    calcPixelsInUnits() {
        let range = this.end - this.start;
        let pixelsInUnit = this.sizeWithoutMargin / range;
        return pixelsInUnit;
    }

    /***
     * zamienia piksele na wartość (odwrotność getPixels)
     * @param px
     * @returns {*}
     */
    getPosByPixels(px, round = false) {
        let range = this.end - this.start;
        if (range == 0) {
            range = 1;
        }
        let pixelsInUnit = this.sizeWithoutMargin / range;

        let value;
        if (this.name == 'x')
            value = (px - this.marginStart) / pixelsInUnit + this.start;
        else
            value = -(px - this.marginEnd) / pixelsInUnit + this.end;
        if (round) {
            value = Math.round(value / this.getUnitsInStep()) * this.getUnitsInStep();
        }
        return value;
    }

    getPosByPixelsOptimized() {
        let range = this.end - this.start;
        if (range == 0) {
            range = 1;
        }

        let pixelsInUnit = this.sizeWithoutMargin / range;
        let marginStart = this.marginStart;
        let marginEnd = this.marginEnd;
        let start = this.start;
        let end = this.end;
        if (this.name == 'x')
            return px => (px - marginStart) / pixelsInUnit + start;
        else
            return px => -(px - marginEnd) / pixelsInUnit + end;

    }

    redrawedGrid() {
        this._lastDrawedGridConfig = this._configHash();
    }

    createValue(number) {
        return new Value(number, this.getConfigValue('defaultUnitName'), this.getConfigValue('unit'));
    }
}

module.exports.Axis = Axis;