var expect = require("chai").expect;
const {Plane} = require("../js/Plane");
const {Axis} = require("../js/Axis");

//mockup
window = {
    addEventListener: () => {
    }, requestAnimationFrame: () => {
    }
};


describe("Plane", function () {

    describe("Config", function () {

        it("changing", function () {

            var a = new Plane({x: {start: 5, end: 6, step: 30}, y: {start: 1}});
            a.xs.config = {end: 7};
            a.config = {y: {end: 7}};

            expect(a.config.x.start).to.equal(5);
            expect(a.config.x.end).to.equal(6);
            expect(a.config.x.step).to.equal(30);
            expect(a.config.y.start).to.equal(1);
            expect(a.config.y.end).to.equal(7);
            expect(a.xs.config.end).to.equal(7);
            expect(a.ys.config.end).to.equal(7);


            expect(a.xs.config.start).to.equal(5);
            expect(a.xs.config.end).to.equal(7);
            expect(a.xs.config.step).to.equal(30);
            expect(a.ys.config.start).to.equal(1);
            expect(a.ys.config.end).to.equal(7);

        });
    });

    describe("Event", function () {
        it("mouseMove", function () {
            var a = new Plane();
            var oldLength = a._mouseMoveEvents.length;
            var fun = event => {
            };
            a.mouseMove(fun);
            expect(a._mouseMoveEvents.length).to.equals(oldLength + 1);
        });
        it("mouseLeave", function () {
            var a = new Plane();
            var oldLength = a._mouseLeaveEvents.length;
            var fun = event => {
            };
            a.mouseLeave(fun);
            expect(a._mouseLeaveEvents.length).to.equals(oldLength + 1);
        });
    });

    describe("Axies", function () {

        it("addAxis removeAxis", function () {

            var a = new Plane();
            a.ys._svg = {
                remove() {
                }
            };//mock
            let testAxis = new Axis('x');
            testAxis._svg = {
                remove() {
                }
            };//mock
            expect(a.containsAxis(testAxis)).to.be.equal(false);
            a.addAxis(testAxis)
            expect(a.containsAxis(testAxis)).to.be.equal(true);
            expect(a._xs[1]).to.equal(testAxis);
            a.removeAxis(testAxis);
            expect(a.containsAxis(testAxis)).to.be.equal(false);
            expect(a._xs.length).to.equal(1);
        });
        it("replace default axis", function () {

            var a = new Plane();
            a.ys._svg = {
                remove() {
                }
            };//mock
            let testAxis = new Axis('y');
            expect(a.containsAxis(testAxis)).to.be.equal(false);
            a.addAxis(testAxis)
            expect(a.containsAxis(testAxis)).to.be.equal(true);
            expect(a.ys).to.not.equal(testAxis);
            expect(a._ys[1]).to.equal(testAxis);
            a.removeAxis(a.ys);
            expect(a.ys).to.equal(testAxis);
            expect(a._ys.length).to.equal(1);
        });
        it("prependicular thickness", function () {
            var a = new Plane();
            a.xs.config = {thickness: 33, marginStart: 2, marginEnd: 6};
            a.ys.config = {thickness: 22, marginStart: 10, marginEnd: 11};
            a._refreshPrependicularThickness();
            expect(a.xs.marginStart).to.equal(24);
            expect(a.xs.marginEnd).to.equal(6);
            expect(a.ys.marginStart).to.equal(43);
            expect(a.ys.marginEnd).to.equal(11);
        });
        it("setMainAxis X", function () {
            var a = new Plane();
            let testAxis = new Axis('x');
            let startAxis=a.xs;
            a.addAxis(testAxis);
            expect(a._xs[0]).to.equal(startAxis);
            expect(a._xs[1]).to.equal(testAxis);
            expect(a._xs.length).to.equal(2);

            a.setMainAxis(testAxis);

            expect(a._xs[0]).to.equal(testAxis);
            expect(a._xs[1]).to.equal(startAxis);
            expect(a._xs.length).to.equal(2);
        });
        it("setMainAxis Y", function () {
            var a = new Plane();
            let testAxis = new Axis('y');
            let startAxis=a.ys;
            a.addAxis(testAxis);
            expect(a._ys[0]).to.equal(startAxis);
            expect(a._ys[1]).to.equal(testAxis);
            expect(a._ys.length).to.equal(2);

            a.setMainAxis(testAxis);

            expect(a._ys[0]).to.equal(testAxis);
            expect(a._ys[1]).to.equal(startAxis);
            expect(a._ys.length).to.equal(2);
        });

        it("setMainAxis new", function () {
            var a = new Plane();
            let testAxis = new Axis('y');
            let startAxis=a.ys;
            expect(a._ys[0]).to.equal(startAxis);
            expect(a._ys.length).to.equal(1);

            a.setMainAxis(testAxis);

            expect(a._ys[0]).to.equal(testAxis);
            expect(a._ys[1]).to.equal(startAxis);
            expect(a._ys.length).to.equal(2);
        });
    });

});