var expect = require("chai").expect;
const {Line} = require("../js/Line");

describe("Line", function () {

    describe("Config", function () {

        it("changing", function () {

            var a = new Line(x => x, {start: 5, end: 6, step: 30});
            a.config = {end: 7};
            expect(a.config.start).to.equal(5);
            expect(a.config.end).to.equal(7);
            expect(a.config.step).to.equal(30);

        });
    });


});