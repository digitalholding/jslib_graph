var expect = require("chai").expect;
const {Axis} = require("../js/Axis");

describe("Axis", function () {
    describe("getPosByPixels", function () {
        it("is it reversable on x?", function () {

            var a = new Axis('x', {start: -10 - Math.random() * 10, end: Math.random() * 20});
            a.size = 100 + Math.round(Math.random() * 100);

            expect(a.getPixels(a.getPosByPixels(500))).to.be.closeTo(500, 0.01);

        });
        it("is it reversable on y?", function () {

            var a = new Axis('y', {start: -10 - Math.random() * 10, end: Math.random() * 20});
            a.size = 100 + Math.round(Math.random() * 100);

            expect(a.getPixels(a.getPosByPixels(50))).to.be.closeTo(50, 0.01);

        });
        it("is it reversible on default?", function () {

            var a = new Axis('x');
            for (var i = 0; i < 10; i++) {

                expect(a.getPixels(a.getPosByPixels(i))).to.be.closeTo(i, 0.01);
            }
        });
        it("is it reversable on smartScale?", function () {

            var a = new Axis('x', {start: -10 - Math.random() * 10, end: Math.random() * 20, smartScale: true});
            a.size = 100 + Math.round(Math.random() * 100);

            expect(a.getPixels(a.getPosByPixels(50))).to.be.closeTo(50, 0.01);

        });

        it("rounding?", function () {

            var a = new Axis('x');
            a.config = {step: 5, start: 0, end: 100, marginStart: 0, marginEnd: 0};
            a.size = 1000;
            expect(a.getPosByPixels(0, true)).to.equal(0);

            expect(a.getPosByPixels(1, true)).to.equal(0);
            expect(a.getPosByPixels(1, false)).to.equal(0.1);


            expect(a.getPosByPixels(4, true)).to.equal(0.5);
            expect(a.getPosByPixels(4, false)).to.equal(0.4);
        });

        describe("customUnit", function () {
            it("is it reversable on x?", function () {

                var a = new Axis('x', {
                    start: -10 - Math.random() * 10,
                    end: Math.random() * 20,
                    unitApply: x => 2 * x + 1,
                    unitRestore: x => (x - 1) / 2
                });
                a.size = 100 + Math.round(Math.random() * 100);

                expect(a.getPixels(a.getPosByPixels(50))).to.be.closeTo(50, 0.01);

            });
            it("is it reversable on y?", function () {

                var a = new Axis('y', {
                    start: -10 - Math.random() * 10,
                    end: Math.random() * 20,
                    unitApply: x => 2 * x + 1,
                    unitRestore: x => (x - 1) / 2
                });
                a.size = 100 + Math.round(Math.random() * 100);

                expect(a.getPixels(a.getPosByPixels(50))).to.be.closeTo(50, 0.01);

            });
            it("is it reversable on smartScale?", function () {

                var a = new Axis('x', {
                    start: -10 - Math.random() * 10,
                    end: Math.random() * 20,
                    smartScale: true,
                    unitApply: x => 2 * x + 1,
                    unitRestore: x => (x - 1) / 2
                });
                a.size = 100 + Math.round(Math.random() * 100);

                expect(a.getPixels(a.getPosByPixels(50))).to.be.closeTo(50, 0.01);

            });
        });
    });

    describe("start-end", function () {
        it("standard", function () {
            var a = new Axis('y', {start: 5, end: 6, step: 30});
            expect(a.start).to.equal(5);
            expect(a.end).to.equal(6);
        });
        it("smartScale", function () {
            var a = new Axis('y', {start: 5.3, end: 5.7, marginStart: 0, marginEnd: 0, step: 50, smartScale: false});
            a.size = 100;
            expect(a.start).to.equal(5.3);
            expect(a.end).to.equal(5.7);
            a.config = {smartScale: true}
            expect(a.start).to.equal(5);
            expect(a.end).to.equal(6);
        });
    });
    describe("unitsInStep", function () {

        it("basic usage", function () {
            var a = new Axis('x');
            let randomMargin = Math.round(Math.random() * 100);
            a.config = {step: 5, start: 0, end: 100, marginStart: randomMargin, marginEnd: randomMargin};
            a.size = 100 + randomMargin * 2;
            expect(a.getUnitsInStep()).to.equal(5);
            expect(a.getUnitsInStep(2)).to.equal(2.5);
        });
        it("rounding", function () {
            var a = new Axis('x');
            a.config = {step: 5, start: 0, end: 100, marginStart: 0, marginEnd: 0};
            a.size = 110;
            expect(a.getUnitsInStep()).to.equal(5);
            expect(a.getUnitsInStep(2)).to.equal(2.5);
        });

        it("other numbers", function () {
            var a = new Axis('x');
            a.config = {step: 12, start: 1, end: 3, marginStart: 0, marginEnd: 0};
            a.size = 555;
            expect(a.getUnitsInStep()).to.equal(0.05);
            expect(a.getUnitsInStep(4)).to.equal(0.0125);
        });

        describe("customUnits", function () {
            it("basic usage", function () {
                var a = new Axis('x');
                let randomMargin = Math.round(Math.random() * 100);
                a.config = {
                    step: 5,
                    start: 0,
                    end: 100,
                    marginStart: randomMargin,
                    marginEnd: randomMargin,
                    unitApply: x => x * 1000,
                    unitRestore: x => x / 1000
                };
                a.size = 100 + randomMargin * 2;
                expect(a.getUnitsInStep()).to.equal(5);

                a.config = {
                    unit: {
                        apply: x => x * 1.1,
                        restore: x => x / 1.1
                    }
                };
                expect(a.getUnitsInStep()).to.equal(10 / 1.1);
            });
        });
    });
    describe("getPixels", function () {

        it("is it reversible on default?", function () {

            var a = new Axis('x');
            var randNumber = Math.random();
            expect(a.getPosByPixels(a.getPixels(randNumber))).to.be.closeTo(randNumber, 0.01);

        });
    });
    describe("Config", function () {

        it("changing", function () {

            var a = new Axis('y', {start: 5, end: 6, step: 30});
            a.config = {end: 7};
            expect(a.config.start).to.equal(5);
            expect(a.config.end).to.equal(7);
            expect(a.config.step).to.equal(30);

        });
        it("parent", function () {

            var a = new Axis('y');
            let rand = Math.random();
            let rand2 = Math.random();
            a.parentConfig = {start: rand, end: 0};
            expect(a.config.start).to.equal(rand);
            expect(a.config.end).to.equal(0);
            a.config = {start: rand2};
            expect(a.config.start).to.equal(rand2);
            expect(a.config.end).to.equal(0);

        });
    });

    it("round", function () {
        expect(Axis.round(9)).to.equal(10);
        expect(Axis.round(25)).to.equal(50);
        expect(Axis.round(.3)).to.equal(.5);
        expect(Axis.round(.19)).to.equal(.2);
        expect(Axis.round(1)).to.equal(1);
    });

    describe("Margin", function () {

        it("basic", function () {
            var a = new Axis('x');
            a.config = {marginStart: 10, marginEnd: 20};
            expect(a.marginStart).to.equal(10);
            expect(a.marginEnd).to.equal(20);
        });
        it("prependicular", function () {
            var x = new Axis('x');
            x.config = {marginStart: 10, marginEnd: 20};
            x.marginAdditionalStart = 25;
            x.marginAdditionalEnd = 45;
            expect(x.marginStart).to.equal(35);
            expect(x.marginEnd).to.equal(65);
            x.size = 200;
            expect(x.sizeWithoutMargin).to.equal(200 - 35 - 65);
        });
    });

    describe("startLimit/endLimit", function () {
        it("basic", function () {
            var a = new Axis('x');
            a.config = {start: -5, end: 20};
            expect(a.start).to.equal(-5);
            expect(a.end).to.equal(20);

            a.config = {startLimit: 0, endLimit: 50};
            expect(a.start).to.equal(0);
            expect(a.end).to.equal(20);

            a.config = {startLimit: Number.NEGATIVE_INFINITY, endLimit: 5};
            expect(a.start).to.equal(-5);
            expect(a.end).to.equal(5);
        });
        it("smartScale", function () {
            var a = new Axis('y', {start: 0.1, end: 0.9, marginStart: 0, marginEnd: 0, step: 50, smartScale: true});
            a.size = 100;
            expect(a.start).to.equal(0);
            expect(a.end).to.equal(1.5);
            a.config = {startLimit: 0, endLimit: 1}
            expect(a.start).to.equal(0);
            expect(a.end).to.equal(1);
        });
    });

});