var expect = require("chai").expect;
const {Point} = require("../js/Point");

describe("Point", function () {

    describe("Config", function () {

        it("changing", function () {

            var a = new Point(x => x, {dummyKey: 30});
            a.config = {dummyKey: 7};
            expect(a.config.dummyKey).to.equal(7);

        });
    });


});